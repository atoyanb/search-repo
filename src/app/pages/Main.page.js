import axios from "axios";
import { Box } from "@chakra-ui/layout";
import { Stack } from "@chakra-ui/layout";
import { Center } from "@chakra-ui/layout";
import { useEffect, useRef, useState } from "react";
import { Spinner } from "@chakra-ui/spinner";
import { paginate } from "../utils/paginate";
import MainLayout from "../layouts/Main.layout";
import Repository from "../components/Repository";
import { useBottomScrollListener } from "react-bottom-scroll-listener";
import { Text } from "@chakra-ui/layout";
import { useToast } from "@chakra-ui/toast";


let currentPage = 0;

const MainPage = () => {
  const toast = useToast();
  const fetchRepos = useRef(() => {});
  const [repos, setRepos] = useState([]);
  const [fetching, setFetching] = useState(true);
  const [filteredRepos, setFilteredRepos] = useState([]);

  fetchRepos.current = async () => {
    try {
      const { data } = await axios.get(
        "https://api.github.com/search/repositories?q=stars:%3E=1000&sort=stars&order=desc&per_page=50"
      );
      const paginated = paginate(data?.items);
      setRepos(paginated);
      setFetching(false);
      setFilteredRepos(paginated[currentPage]);
    } catch (error) {
      console.error(error);
      setRepos([]);
      setFilteredRepos([]);
      setFetching(false);
      toast({
        title: "There was an error",
        duration: 5000,
        isClosable: false,
        status: "error",
      });
    }
  };

  useEffect(() => {
    fetchRepos.current();
    return () => {};
  }, []);

  useBottomScrollListener(() => {
    if (currentPage + 1 < repos.length) {
      currentPage += 1;
      setFilteredRepos(filteredRepos.concat(repos[currentPage]));
    }

    return () => {};
  });

  return (
    <MainLayout>
      <Box>
        <Stack spacing={5}>
          <Stack spacing={10}>
            <Box>
              {fetching ? (
                <Center>
                  <Spinner />
                </Center>
              ) : filteredRepos?.length === 0 || repos?.length === 0 ? (
                <Center>
                  <Text fontWeight={"semibold"} fontSize={"lg"}>
                    Nope, nothing here 🤔
                  </Text>
                </Center>
              ) : (
                <Stack spacing={5}>
                  {filteredRepos?.map((repo) => {
                    return <Repository key={repo?.id} data={repo} />;
                  })}
                </Stack>
              )}
            </Box>
          </Stack>
        </Stack>
      </Box>
    </MainLayout>
  );
};

export default MainPage;
