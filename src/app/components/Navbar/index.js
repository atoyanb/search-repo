import { IconButton } from "@chakra-ui/button";
import { Stack } from "@chakra-ui/layout";
import { Link } from "@chakra-ui/layout";
import { Box, Center, Flex, Spacer, Text } from "@chakra-ui/layout";
import { Tooltip } from "@chakra-ui/tooltip";
import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { AiOutlineGitlab } from "react-icons/ai"

const Title = () => {
  return (
    <Center as={NavLink} to={"/"}>
      <Text fontWeight={"semibold"} fontSize={"xl"}>
        Popular GitHub Repos
      </Text>
    </Center>
  );
};

const GitLabButton = () => {
  return (
    <Center>
      <Tooltip label={"GitLab"}>
        <Link
          target={"_blank"}
          rel={"noopener noreferrer"}
          href={"https://gitlab.com/atoyanb"}
        >
          <IconButton icon={<AiOutlineGitlab />} isRound size={"md"} />
        </Link>
      </Tooltip>
    </Center>
  );
};

const Buttons = () => {
  return (
    <Box>
      <Stack direction={"row"}>
        <GitLabButton />
      </Stack>
    </Box>
  );
};

const Navbar = () => {
  const [scrolled, setScrolled] = useState(false);

  useEffect(() => {
    const handle = window.addEventListener("scroll", () => {
      if (window.scrollY > 10) setScrolled(true);
      else setScrolled(false);
    });

    return window.removeEventListener("scroll", handle);
  }, []);

  return (
    <Box
      top={0}
      py={[2, 3]}
      pos={"sticky"}
      zIndex={"sticky"}
      borderBottom={"2px"}
      transition={"ease-in-out 200ms"}
      px={[3, 50, 100, 150, 250, 300]}
      boxShadow={scrolled ? "lg" : null}
      bgColor={"gray.50"}
      borderColor={"gray.100"}
    >
      <Box transition={"ease-in-out 150ms"} p={scrolled ? 1 : null}>
        <Flex>
          <Title />
          <Spacer />
          <Buttons />
        </Flex>
      </Box>
    </Box>
  );
};

export { Navbar };
