const paginate = (arr = [], size = 10) => {
  if (arr.length === size) {
    return arr;
  } else {
    let mutableSize = 0;
    let paginatedArray = [];
    let mutablePaginatedArray = [];

    for (let x = 0; x < arr.length; x++) {
      if (mutableSize < size) {
        mutablePaginatedArray.push(arr[x]);
        mutableSize++;
      } else {
        paginatedArray.push(mutablePaginatedArray);
        mutablePaginatedArray = [];
        mutableSize = 0;
      }
    }

    return paginatedArray;
  }
};

export { paginate };
